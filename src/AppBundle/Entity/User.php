<?php

/* 
 * Copyright (C) 2017 The Art Of Soft
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * User "special" entity
 * 
 * @ORM\Table(name="app_users")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\UserRepository")
 * @UniqueEntity(fields="email", message="Email already taken")
 * @UniqueEntity(fields="username", message="Username already taken")
 */
class User implements UserInterface, \Serializable
{
  /**
   * @var int
   *
   * @ORM\Column(type="integer")
   * @ORM\Id
   * @ORM\GeneratedValue(strategy="AUTO")
   */
  private $id;

  /**
   * @var string
   *
   * @ORM\Column(type="string", length=255, unique=true)
   * @Assert\NotBlank()
   */
  private $username;

  /**
   * @Assert\NotBlank()
   * @Assert\Length(max=4096)
   */
  private $plainPassword;

  /**
   * @var string
   *
   * @ORM\Column(type="string", length=64)
   */
  private $password;

  /**
   * @var string
   *
   * @ORM\Column(type="string", length=255, unique=true)
   * @Assert\NotBlank()
   * @Assert\Email()
   */
  private $email;

  /**
   * @var array
   *
   * @ORM\Column(type="json_array")
   */
  private $roles = [];

  public function getUsername()
  {
    return $this->username;
  }

  public function getPlainPassword()
  {
    return $this->plainPassword;
  }

  public function setPlainPassword($password)
  {
    $this->plainPassword = $password;
    //$this->password = null;
  }

  public function getPassword()
  {
    return $this->password;
  }

  public function getSalt()
  {
    // The bcrypt algorithm doesn't require a separate salt.
    // You *may* need a real salt if you choose a different encoder.
    return null;
  }

  /**
   * Returns the roles or permissions granted to the user for security.
   */
  public function getRoles()
  {
    $roles = $this->roles;

    // guarantees that a user always has at least one role for security
    if (empty($roles))
    {
      $roles[] = 'ROLE_USER';
    }

    return array_unique($roles);
  }

  public function setRoles(array $roles)
  {
    if (empty($roles))
    {
      $roles[] = 'ROLE_USER';
    }

    $this->roles = $roles;
  }

  public function eraseCredentials()
  {
    $this->plainPassword = null;
  }

  /** @see \Serializable::serialize() */
  public function serialize()
  {
    return serialize([$this->id, $this->username, $this->password]);
  }

  /** @see \Serializable::unserialize() */
  public function unserialize($serialized)
  {
    list ($this->id, $this->username, $this->password) = 
            unserialize($serialized);
  }

  public function getId()
  {
    return $this->id;
  }

  public function setUsername($username)
  {
    $this->username = $username;
    return $this;
  }

  public function setPassword($password)
  {
    $this->password = $password;
    return $this;
  }

  public function setEmail($email)
  {
    $this->email = $email;
    return $this;
  }

  public function getEmail()
  {
    return $this->email;
  }

}
