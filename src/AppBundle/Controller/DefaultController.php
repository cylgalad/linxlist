<?php
/* 
 * Copyright (C) 2017 The Art Of Soft
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace AppBundle\Controller;

use AppBundle\Entity\User;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Cache;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Controller for the default homepage
 * @author cylgalad <cylgalad@gmail.com>
 */
class DefaultController extends Controller
{
  /**
   * @Route("/", name="homepage")
   * @param Request $request Request
   */
  public function indexAction(Request $request)
  {
    if ($this->get('security.authorization_checker')
             ->isGranted('IS_AUTHENTICATED_FULLY'))
    {
      return $this->redirectToRoute('categorybase');
    }

    $em = $this->getDoctrine()->getManager();
    $userlist = $em->getRepository('AppBundle:User')->findAll();
    if (empty($userlist))
    {
      return $this->redirectToRoute('user_registration');
    }

    return $this->render('default/index.html.twig');
  }
}