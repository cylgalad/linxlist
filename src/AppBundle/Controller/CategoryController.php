<?php

/* 
 * Copyright (C) 2017 The Art Of Soft
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace AppBundle\Controller;

use AppBundle\Entity\User;
use AppBundle\Entity\Category;
use AppBundle\Entity\Link;
use AppBundle\Form\CategoryType;
use AppBundle\Form\LinkType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\User\UserInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

/**
 * Controller for all category and link views
 * @author cylgalad <cylgalad@gmail.com>
 */
class CategoryController extends Controller
{
  /**
   * @Route("/category", name="categorybase")
   * @Security("is_granted('ROLE_USER')")
   * @param Request $request Request
   * @return Response Response
   */
  public function categorybaseAction(Request $request)
  {
    $user = $this->get('security.token_storage')->getToken()->getUser();
    $em = $this->getDoctrine()->getManager();
    $catlist = $em->getRepository('AppBundle:Category')
                  ->findBy(['user' => $user]);

    $session = $request->getSession();
    $curid = $session->get('curid', null);
    $idx = 0;
    if ($curid !== null)
    {
      foreach ($catlist as $k => $v)
      {
        if ($v->getId() == $curid)
        {
          $idx = $k;
          break;
        }
      }
    }

    return $this->redirectToRoute('category',
            ['name' => $catlist[$idx]->getName()]);
  }

  /**
   * @Route("/category/{name}", name="category")
   * @Security("is_granted('ROLE_USER')")
   * @param Request $request Request
   * @param string $name Category name
   * @return Response Response
   */
  public function categoryAction(Request $request, $name)
  {
    $user = $this->get('security.token_storage')->getToken()->getUser();
    $em = $this->getDoctrine()->getManager();
    $cat = $em->getRepository('AppBundle:Category')
              ->findOneBy(['user' => $user, 'name' => $name]);
    // If category doesn't exist
    if(!$cat)
    {
      $this->addFlash('danger', $this->get('translator')
           ->trans('msg.catnotfound', ['%name%' => $name]));
      return $this->redirectToRoute('categorybase');
    }

    $catlist = $em->getRepository("AppBundle:Category")
                  ->findBy(['user' => $user]);

    $linklist = $em->getRepository('AppBundle:Link')
                   ->findBy(['category' => $cat]);

    $session = $request->getSession();
    $session->set('curid', $cat->getId());

    return $this->render('category/category.html.twig',
            ['user' => $user,
             'curcat' => $cat,
             'catlist' => $catlist,
             'linklist' => $linklist]);
  }

  /**
   * @Route("/new/category", name="newcat")
   * @Security("is_granted('ROLE_USER')")
   * @param Request $request Request
   * @return Response Response
   */
  public function newcatAction(Request $request)
  {
    $user = $this->get('security.token_storage')->getToken()->getUser();
    $cat = new Category();
    $form = $this->createForm(CategoryType::class, $cat);

    $form->handleRequest($request);
    if ($form->isSubmitted() && $form->isValid())
    {
      $cat->setUser($user);
      $em = $this->getDoctrine()->getManager();
      $em->persist($cat);
      $em->flush();
      $this->addFlash('success', $this->get('translator')
              ->trans('msg.newcat_success', ['%name%' => $cat->getName()]));
      return $this->redirectToRoute('category', ['name' => $cat->getName()]);
    }

    return $this->render('category/newcat.html.twig',
            ['form' => $form->createView()]);
  }

  /**
   * @Route("/edit/category/{id}", name="editcat")
   * @Security("is_granted('ROLE_USER')")
   * @param Request $request Request
   * @param Category $cat Category to edit
   * @return Response Response
   */
  public function editcatAction(Request $request, Category $cat)
  {
    $user = $this->get('security.token_storage')->getToken()->getUser();
    $em = $this->getDoctrine()->getManager();
    $cat2 = $em->getRepository('AppBundle:Category')
               ->findOneBy(['user' => $user, 
                            'name' => $cat->getName()]);
    // If category doesn't exist
    if(!$cat2)
    {
      $this->addFlash('danger', $this->get('translator')
           ->trans('msg.catnotfound', ['%name%' => $cat->getName()]));
      return $this->redirectToRoute('categorybase');
    }

    $form = $this->createForm(CategoryType::class, $cat);
    $form->handleRequest($request);
    if ($form->isSubmitted() && $form->isValid())
    {
      $cat = $form->getData();
      $cat->setUser($user);
      $em->persist($cat);
      $em->flush();
      $this->addFlash('success', $this->get('translator')
              ->trans('msg.editcat_success', ['%name%' => $cat->getName()]));
      return $this->redirectToRoute('categorybase');
    }

    return $this->render('category/editcat.html.twig',
            ['form' => $form->createView()]);
  }

  /**
   * @Route("/del/category/{id}", name="delcat")
   * @Security("is_granted('ROLE_USER')")
   * @param Request $request Request
   * @param Category $cat Category to delete
   * @return Response Response
   */
  public function delcatAction(Request $request, Category $cat)
  {
    $user = $this->get('security.token_storage')->getToken()->getUser();
    $em = $this->getDoctrine()->getManager();
    $cat2 = $em->getRepository('AppBundle:Category')
               ->findOneBy(['user' => $user,
                            'id' => $cat->getId()]);
    // If category doesn't exist
    if(!$cat2)
    {
      $this->addFlash('danger', $this->get('translator')
           ->trans('msg.catnotfound', ['%name%' => $cat->getName()]));
      return $this->redirectToRoute('categorybase');
    }

    $form = $this->createFormBuilder()->getForm();
    $form->handleRequest($request);
    if ($form->isSubmitted() && $form->isValid())
    {
      $session = $request->getSession();
      $curid = $session->get('curid', null);
      if ($curid === $cat->getId())
      {
        $session->set('curid', null);
      }

      $name = $cat->getName();
      $em->remove($cat);
      $em->flush();
      $this->addFlash('success', $this->get('translator')
              ->trans('msg.delcat_success', ['%name%' => $name]));
      return $this->redirectToRoute('categorybase');
    }

    return $this->render('category/delcat.html.twig',
            ['cat' => $cat,
             'form' => $form->createView()]);
  }
  
  /**
   * @Route("/new/link/{id}", name="newlink")
   * @Security("is_granted('ROLE_USER')")
   * @param Request $request Request
   * @param Category $cat Category of the new link
   * @return Response Response
   */
  public function newlinkAction(Request $request, Category $cat)
  {
    $user = $this->get('security.token_storage')->getToken()->getUser();
    $em = $this->getDoctrine()->getManager();
    $cat2 = $em->getRepository('AppBundle:Category')
              ->findOneBy(['user' => $user, 'id' => $cat->getId()]);
    // If category doesn't exist
    if(!$cat2)
    {
      $this->addFlash('danger', $this->get('translator')
           ->trans('msg.catnotfound', ['%name%' => $cat->getName()]));
      return $this->redirectToRoute('categorybase');
    }

    $link = new Link();
    $form = $this->createForm(LinkType::class, $link);

    $form->handleRequest($request);
    if ($form->isSubmitted() && $form->isValid())
    {
      $link->setCategory($cat2);
      $em->persist($link);
      $em->flush();
      $this->addFlash('success', $this->get('translator')
              ->trans('msg.newlink_success', ['%name%' => $link->getName()]));
      return $this->redirectToRoute('category', ['name' => $cat->getName()]);
    }

    return $this->render('category/newlink.html.twig',
            ['form' => $form->createView(), 'cat' => $cat]);
  }

  /**
   * @Route("/edit/link/{id}", name="editlink")
   * @Security("is_granted('ROLE_USER')")
   * @param Request $request Request
   * @param Link $link Link to edit
   * @return Response Response
   */
  public function editlinkAction(Request $request, Link $link)
  {
    $form = $this->createForm(LinkType::class, $link);

    $form->handleRequest($request);
    if ($form->isSubmitted() && $form->isValid())
    {
      $link = $form->getData();
      $em = $this->getDoctrine()->getManager();
      $em->persist($link);
      $em->flush();
      $this->addFlash('success', $this->get('translator')
              ->trans('msg.newlink_success', ['%name%' => $link->getName()]));
      return $this->redirectToRoute('category',
              ['name' => $link->getCategory()->getName()]);
    }

    return $this->render('category/editlink.html.twig',
            ['form' => $form->createView(), 'cat' => $link->getCategory()]);
  }

  /**
   * @Route("/del/link/{id}", name="dellink")
   * @Security("is_granted('ROLE_USER')")
   * @param Request $request Request
   * @param Link $link Link to delete
   * @return Response Response
   */
  public function dellinkAction(Request $request, Link $link)
  {
    $em = $this->getDoctrine()->getManager();
    $lnk2 = $em->getRepository('AppBundle:Link')
               ->findOneBy(['id' => $link->getId()]);
    // If link doesn't exist
    if(!$lnk2)
    {
      $this->addFlash('danger', $this->get('translator')
           ->trans('msg.linknotfound', ['%name%' => $link->getName()]));
      return $this->redirectToRoute('categorybase');
    }

    $form = $this->createFormBuilder()->getForm();
    $form->handleRequest($request);
    if ($form->isSubmitted() && $form->isValid())
    {
      $name = $link->getName();
      $catname = $link->getCategory()->getName();
      $em->remove($link);
      $em->flush();
      $this->addFlash('success', $this->get('translator')
              ->trans('msg.dellink_success', ['%name%' => $name]));
      return $this->redirectToRoute('category', ['name' => $catname]);
    }

    return $this->render('category/dellink.html.twig',
            ['lnk' => $link, 'form' => $form->createView()]);
  }

}